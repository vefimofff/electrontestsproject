var keyMirror = require('keymirror');

module.exports = keyMirror({
    CREATE_SECTIONS: null,
    SET_DISPLAYED_QUESTION_INDEX: null,
    LOGIN_UPDATE_FIRST_NAME: null,
    LOGIN_UPDATE_LAST_NAME: null,
    LOGIN_UPDATE_MIDDLE_NAME: null,
    LOGIN_UPDATE_CATEGORY: null,
    LOGIN_UPDATE_DAY_OF_BIRTH: null,
    LOGIN_UPDATE_MONTH_OF_BIRTH: null,
    LOGIN_UPDATE_YEAR_OF_BIRTH: null,
    EVALUATE_QUESTION: null,
    CHOICE_CLICKED: null,
    CREATE_PDF_WITH_RESULTS: null
});
