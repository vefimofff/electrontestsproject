How to install project dependencies
=======================================
cd <project directory pathname>
sudo npm install --registry http://registry.npmjs.eu/

How to run project
=======================================
Step 1.
    cd <project directory pathname>
    npm run watch

Step 2 (in another Terminal window)
    cd <project directory pathname>
    npm start
