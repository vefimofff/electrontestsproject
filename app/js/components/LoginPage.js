import React from 'react';
import { Button, Input } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import NameInput  from './NameInput'
import DateInput  from './DateInput'
import TestsActions from '../actions/TestsActions'
import EventsActions from '../actions/EventsActions'
import TestsStore from '../stores/TestsStore'
import EventsStore from '../stores/EventsStore'
import { PropTypes } from 'react-router'

function isEmptyName(name) {
    return name ? !(/\S/.test(name)) : true;
}

function isEmptyDate(date) {
    return date ? date == "0" : true;
}

export default
class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            firstNameEmpty: false, lastNameEmpty: false, middleNameEmpty: false,
            dayOfBirthEmpty: false, monthOfBirthEmpty: false, yearOfBirthEmpty: false
        };
    }

    isInputDataCorrect() {
        var isInputDataCorrect = true;
        if (isEmptyName(TestsStore.getFirstName()) == true) {
            isInputDataCorrect = false;
            this.setState({firstNameEmpty: true});
        } else {
            this.setState({firstNameEmpty: false});
        }
        if (isEmptyName(TestsStore.getLastName()) == true) {
            isInputDataCorrect = false;
            this.setState({lastNameEmpty: true});
        } else {
            this.setState({lastNameEmpty: false});
        }
        if (isEmptyName(TestsStore.getMiddleName()) == true) {
            isInputDataCorrect = false;
            this.setState({middleNameEmpty: true});
        } else {
            this.setState({middleNameEmpty: false});
        }
        if (isEmptyDate(TestsStore.getDayOfBirth()) == true) {
            isInputDataCorrect = false;
            this.setState({dayOfBirthEmpty: true});
        } else {
            this.setState({dayOfBirthEmpty: false});
        }
        if (isEmptyDate(TestsStore.getMonthOfBirth()) == true) {
            isInputDataCorrect = false;
            this.setState({monthOfBirthEmpty: true});
        } else {
            this.setState({monthOfBirthEmpty: false});
        }
        if (isEmptyDate(TestsStore.getYearOfBirth()) == true) {
            isInputDataCorrect = false;
            this.setState({yearOfBirthEmpty: true});
        } else {
            this.setState({yearOfBirthEmpty: false});
        }

        return isInputDataCorrect;
    }

    //check that data has been input
    enterTest() {
        if (this.isInputDataCorrect()) {
            //searching for user in event
            EventsActions.readEventsFromFile();
            var currentDate = new Date();
            // adding 1 because getMonth from Date() returns value starting from 0
            // and month values in event starts from 1
            var currentYear = currentDate.getFullYear();
            var currentMonth = currentDate.getMonth() + 1;
            var currentDay = currentDate.getDate();


            //TODO DELETE 3 lines below only for testing!
            var currentYear = currentDate.getFullYear();
            var currentMonth = 1;
            var currentDay = 1;



            var categoryForUserFromEvent = "";
            var events = EventsStore.getEvents();
            console.log('events.length=' + events.length);

            for (var eventIndex = 0; eventIndex < events.length; eventIndex++) {
                //todo VE refactor. remove this [0]
                var event = events[eventIndex][0];
                console.log('event.year=' + event.year);
                if (event.year == currentYear) {
                    console.log('Found event with year=' + currentYear);
                    if (event.month == currentMonth) {
                        console.log('Found event with month=' + currentMonth);
                        if (event.day == currentDay) {
                            console.log('Found event with day=' + currentDay);
                            for (var userIndex = 0; userIndex < event.users.length; userIndex++) {
                                var user = event.users[userIndex];
                                if (user.lastName === TestsStore.getLastName()) {
                                    console.log('Found user lastName=' + user.lastName);
                                    if (user.firstName === TestsStore.getFirstName()) {
                                        console.log('Found user firstName=' + user.firstName);
                                        if (user.middleName === TestsStore.getMiddleName()) {
                                            console.log('Found user middleName=' + user.middleName);
                                            if (user.yearOfBirth === TestsStore.getYearOfBirth()) {
                                                console.log('Found user yearOfBirth=' + user.yearOfBirth);
                                                if (user.monthOfBirth === TestsStore.getMonthOfBirth()) {
                                                    console.log('Found user monthOfBirth=' + user.monthOfBirth);
                                                    if (user.dayOfBirth === TestsStore.getDayOfBirth()) {
                                                        console.log('User found!!! user.category=' + user.category);
                                                        categoryForUserFromEvent = user.category;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (categoryForUserFromEvent != "") {
                TestsActions.updateCategory(categoryForUserFromEvent);

                //redirect to test view
                this.context.history.pushState(null, '/test')
            } else {
                //error case
            }
        }
    }

    render() {
        return (
                <div>
                    <div className="loginDiv">
                        <div className="headerDivFont">Вход в систему</div>

                        <p>Введите свои фамилию, имя, отчество и дату рождения, затем нажмите кнопку "Войти в систему".</p>

                        <div className="marginTop20">
                            <NameInput label="Фамилия" initialValue="введите фамилию" nameType="last_name" error={this.state.lastNameEmpty}/>
                            <NameInput label="Имя" initialValue="введите имя" nameType="first_name" error={this.state.firstNameEmpty}/>
                            <NameInput label="Отчество" initialValue="введите отчество" nameType="middle_name" error={this.state.middleNameEmpty}/>
                            <DateInput errorDay={this.state.dayOfBirthEmpty} errorMonth={this.state.monthOfBirthEmpty} errorYear={this.state.yearOfBirthEmpty}/>
                        </div>

                        <div className="loginEnterSystemButtonDiv">
                            <Button bsStyle="primary" onClick={this.enterTest.bind(this)}>Войти в систему</Button>
                        </div>
                        {this.props.children}

                    </div>
                </div>
        );
    }

}

LoginPage.contextTypes = {history: PropTypes.history};