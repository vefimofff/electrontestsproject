import AppDispatcher from '../dispatcher/AppDispatcher';
import TestsConstants from '../constants/TestsConstants';

var fs = remote.require('fs');
var console = remote.require('console');

var resultsFilePath = "../results/";
var _ = require('lodash');
var PDF = remote.require('pdfkit');

var excelBuilder = remote.require('msexcel-builder');

function makeDirectoryIfNotExist(dirPath) {
    try {
        fs.mkdirSync(dirPath);
    } catch (err) {
    }
}

function createPdfWithResults(questions, evaluatedQuestions, fullName, dateOfBirth, category) {
    makeDirectoryIfNotExist(resultsFilePath);

    var doc = new PDF();
    var fileName = fullName + " " + dateOfBirth +".pdf";
    doc.pipe(fs.createWriteStream(resultsFilePath + fileName));

    doc.registerFont('Roboto', '../fonts/RobotoRegular.ttf');
    doc.font('Roboto');

    doc.text("Результаты теста для: ");
    doc.text(fullName);
    doc.text("Дата рождения: " + dateOfBirth);
    doc.text("Категория должности: " + getCategoryInRussian(category));
    doc.moveDown();


    for (var i = 0; i < questions.length; i++) {
        //adding new PDF page for each question
        doc.addPage();
        var question = questions[i];
        var questionNumber = i + 1;

        var resultColor;
        var resultText;
        if (evaluatedQuestions[i].questionResult == "correct") {
            resultColor = "green";
            resultText = "Правильно";
        } else {
            resultColor = "red";
            resultText = "Неправильно";
        }
        doc.text("Вопрос " + questionNumber, {
                    continued: true
                })
                .fillColor(resultColor)
                .text(" - " + resultText);

        doc.moveDown();
        doc.fillColor('black')
                .text(question.title);
        doc.moveDown();

        var correctAnswersArray = [];
        if (question.answer.indexOf(",") > -1) {
            correctAnswersArray = question.answer.split(",");
        } else {
            correctAnswersArray.push(question.answer);
        }

        for (var j = 0; j < question.choices.length; j++) {
            var choice = question.choices[j];
            var choiceNumber = j + 1;

            var fillColor = "black";
            if (correctAnswersArray.indexOf(j.toString()) > -1) {
                fillColor = "green";
            }
            var underline = choice.selected;
            doc.fillColor(fillColor)
                    .text(choiceNumber + ") " + choice.text, {
                        underline: underline
                    })
        }
    }

    doc.end();
}

function createXlsWithResults(questions, evaluatedQuestions, fullName, dateOfBirth, category) {
    makeDirectoryIfNotExist(resultsFilePath);

    var fileName = fullName + " " + dateOfBirth +".xlsx";
    var workbook = excelBuilder.createWorkbook(resultsFilePath, fileName);

    // Create a new worksheet with 5 columns and 120 rows
    var columnsCount = 5;
    var rowsCount = 100;
    var sheet1 = workbook.createSheet('sheet1', columnsCount, rowsCount);

    // Fill some data

    sheet1.set(1, 1, "Результаты теста для: ");
    sheet1.set(2, 1, fullName);
    sheet1.set(1, 2, "Дата рождения: ");
    sheet1.set(2, 2, dateOfBirth);
    sheet1.set(1, 3, "Категория должности: ");
    sheet1.set(2, 3, getCategoryInRussian(category));


    sheet1.set(2, 6, "Вопрос");
    sheet1.set(3, 6, "Результат");
    sheet1.set(4, 6, "Выбранный ответ");
    sheet1.set(5, 6, "Правильный ответ");

    var questionsStartCellNumber = 7;
    for (var i = 0; i < questions.length; i++) {
        var rowNumber = questionsStartCellNumber + i;

        var questionTitle = 'Вопрос ' + (i + 1);
        sheet1.set(2, rowNumber, questionTitle);
        var resultText;
        if (evaluatedQuestions[i].questionResult == "correct") {
            resultText = "Правильно";
        } else {
            resultText = "Неправильно";
        }
        sheet1.set(3, rowNumber, resultText);
        sheet1.set(4, rowNumber, evaluatedQuestions[i].userAnswer);
        sheet1.set(5, rowNumber, evaluatedQuestions[i].correctAnswer);
    }

    // Save it
    workbook.save(function (ok) {
        if (!ok) {
            workbook.cancel();
        }
    });
}

function getCategoryInRussian(category) {
    if (category == "management") {
        return "Руководители";
    } else if (category == "specialists") {
        return "Специалисты";
    } else if (category == "providers") {
        return "Обеспечивающие специалисты";
    } else {
        return "Ошибка определения категории";
    }
}


var ResultActions = {

    createPDFReport: function (questions, evaluatedQuestions, fullName, dateOfBirth, category) {
        createPdfWithResults(questions, evaluatedQuestions, fullName, dateOfBirth, category);

        //AppDispatcher.dispatch({
        //    actionType: TestsConstants.CREATE_PDF_WITH_RESULTS
        //});
    },

    createXLSReport: function (questions, evaluatedQuestions, fullName, dateOfBirth, category) {
        createXlsWithResults(questions, evaluatedQuestions, fullName, dateOfBirth, category);

        //AppDispatcher.dispatch({
        //    actionType: TestsConstants.CREATE_PDF_WITH_RESULTS
        //});
    }

};

module.exports = ResultActions;
