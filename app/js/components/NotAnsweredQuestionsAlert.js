import React, { PropTypes, Component } from 'react';
import { Alert, Button } from 'react-bootstrap'
import TestsStore from '../stores/TestsStore';

export default
class NotAnsweredQuestionsAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {alertVisible: props.show};
    }

    componentWillReceiveProps(nextProps) {
        this.setState({alertVisible: nextProps.show})
    }

    handleAlertDismiss() {
        this.setState({alertVisible: false});
    }

    getSeparatedQuestionNumbers() {
     var skippedQuestionsNumbers = TestsStore.getSkippedQuestionsNumbers();
        var questionsNumbersString = "";
        for (var key in skippedQuestionsNumbers) {
            questionsNumbersString += (questionsNumbersString == "" ? "" : ", ") + skippedQuestionsNumbers[key];
        }
        return questionsNumbersString;
    }

    render() {
        if (this.state.alertVisible) {
            return <div>
                <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss.bind(this)}>
                    <h4>Даны ответы не на все вопросы!</h4>
                    <p>
                        Пожалуйста, выберите ответы для следующих вопросов {this.getSeparatedQuestionNumbers()}.
                    </p>
                    <p>
                        <Button onClick={this.handleAlertDismiss.bind(this)}>Закрыть</Button>
                    </p>
                </Alert>
            </div>;
        } else {
            return <div></div>;
        }
    }
}
