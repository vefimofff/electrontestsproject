import React, {Component} from 'react';
import TestsActions from '../actions/TestsActions'
import TestsStore from '../stores/TestsStore'
import AssessmentNavigator from './AssessmentNavigator'
import QuestionContainer from './QuestionContainer';
import NotAnsweredQuestionsAlert from './NotAnsweredQuestionsAlert';

function getQuestionState() {
    return {
        selectedQuestionIndex: TestsStore.getDisplayedQuestionIndex()
    };
}

function updateSelectedQuestionIndex(newSelectedQuestionIndex) {
    TestsActions.updateDisplayedQuestionIndex(newSelectedQuestionIndex);
}

function evaluateQuestion(selectedQuestionIndex) {
    var question = TestsStore.getQuestion(selectedQuestionIndex);
    var choices = question.choices;
    var correctAnswer = question.answer;
    TestsActions.evaluateQuestion(selectedQuestionIndex, correctAnswer, choices);
}

function getTimeRemaining(endtime) {
    var timeDifferenceMS = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((timeDifferenceMS / 1000) % 60);
    var minutes = Math.floor((timeDifferenceMS / 1000 / 60) % 60);
    var hours = Math.floor((timeDifferenceMS / (1000 * 60 * 60)) % 24);

    minutes = minutes < 10 ? "0" + minutes.toString() : minutes;


    if (seconds < 10) {
        seconds = "0" + seconds.toString();
    }
    return {
        "total": timeDifferenceMS,
        "hours": hours,
        "minutes": minutes,
        "seconds": seconds
    };
}

function initializeClock(id) {
    var timeInMinutes = 60;
    var currentTime = Date.parse(new Date());
    var deadline = new Date(currentTime + timeInMinutes * 60 * 1000);

    var clock = document.getElementById(id);
    var timeInterval = setInterval(function () {
        var time = getTimeRemaining(deadline);
        clock.innerHTML = time.minutes + ' : ' + time.seconds;
        if (time.total <= 0) {
            clearInterval(timeInterval);

            //when the time is over automatically moving to the results page
            //updating question index to the numberOfQuestions (which is +1 to the last question index)
            var indexToEndTest = TestsStore.getNumberOfQuestions();
            updateSelectedQuestionIndex(indexToEndTest);
        }
    }, 1000);
}

export default
class AssessmentView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedQuestionIndex: 0,
            totalNumberOfQuestions: TestsStore.getNumberOfQuestions(),
            showAlert: false
        };
    }

    componentDidMount() {
        TestsStore.addChangeListener(this._onChange.bind(this));
        initializeClock("timer");
    }

    componentWillUnmount() {
        TestsStore.removeChangeListener(this._onChange.bind(this));
    }

    hideAlert() {
        this.setState({showAlert: false});
    }

    showAlert() {
        this.setState({showAlert: true});
    }

    onChoiceClick(selectedQuestionIndex, choiceNumber, inputType) {
        this.hideAlert();
        TestsActions.choiceClicked(selectedQuestionIndex, choiceNumber, inputType);
    }

    nextQuestionAction() {
        this.hideAlert();
        var displayedQuestionIndex = TestsStore.getDisplayedQuestionIndex();
        //evaluating question answer
        evaluateQuestion(displayedQuestionIndex);
        var incrementQuestionIndex = true;
        var lastQuestionIndex = TestsStore.getNumberOfQuestions() - 1;
        if (displayedQuestionIndex == lastQuestionIndex) {
            var skippedQuestionsNumbersArray = TestsStore.getSkippedQuestionsNumbers();
            if (skippedQuestionsNumbersArray.length > 0) {
                this.showAlert();
                incrementQuestionIndex = false;
            }
        }

        //updating question index to display next question
        updateSelectedQuestionIndex(incrementQuestionIndex ? displayedQuestionIndex + 1 : displayedQuestionIndex);
    }

    prevQuestionAction() {
        this.hideAlert();
        var displayedQuestionIndex = TestsStore.getDisplayedQuestionIndex();
        //evaluating question answer
        evaluateQuestion(displayedQuestionIndex);
        //updating question index to display prev question
        updateSelectedQuestionIndex(displayedQuestionIndex - 1);
    }

    onQuestionNumberClick(questionIndex) {
        this.hideAlert();
        var displayedQuestionIndex = TestsStore.getDisplayedQuestionIndex();
        //evaluating question answer
        evaluateQuestion(displayedQuestionIndex);
        updateSelectedQuestionIndex(questionIndex);
    }


    render() {
        var groupTitle = TestsStore.getQuestionGroupTitleForQuestion(this.state.selectedQuestionIndex);
        return <div>
            <NotAnsweredQuestionsAlert show={this.state.showAlert}/>

            <div className="groupDiv">Раздел:<span className="marginLeft5">{groupTitle}</span></div>
            <div className="timerDiv">До конца теста осталось <span id="timer" className="timerFont"/></div>

            <div className="paddingTop25">
                <AssessmentNavigator selectedQuestionIndex={this.state.selectedQuestionIndex}
                                     totalNumberOfQuestions={this.state.totalNumberOfQuestions}
                                     onQuestionNumberClick={this.onQuestionNumberClick.bind(this)}/>
            </div>
            <QuestionContainer questionIndex={this.state.selectedQuestionIndex}
                               prevQuestionAction={this.prevQuestionAction.bind(this)}
                               nextQuestionAction={this.nextQuestionAction.bind(this)}
                               onChoiceClick={this.onChoiceClick.bind(this)}/>
        </div>;


    }

    _onChange() {
        this.setState(getQuestionState())
    }

}