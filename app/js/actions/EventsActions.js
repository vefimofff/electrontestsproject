import AppDispatcher from '../dispatcher/AppDispatcher';
import EventsConstants from '../constants/EventsConstants';

var fs = remote.require('fs');

var eventsFilePath = "../tests/";
var eventsFileName = "events.txt";

function readEventsJsonFromFile() {
    var eventsJsonAsString = fs.readFileSync(eventsFilePath + eventsFileName, 'utf-8');
    return JSON.parse(eventsJsonAsString);
}

var EventsActions = {
    readEventsFromFile: function () {
        var events = readEventsJsonFromFile();

        AppDispatcher.dispatch({
            actionType: EventsConstants.CREATE_EVENTS,
            events: events
        });
    }
};

module.exports = EventsActions;
