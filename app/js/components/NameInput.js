import React, { PropTypes, Component } from 'react';
import { Input } from 'react-bootstrap'
import TestsActions from '../actions/TestsActions';

export default
class NameInput extends Component {
    constructor(props) {
        super(props);
        this.state = {value: props.initialValue, nameType: props.nameType, label: props.label, error: this.props.error};
    }

    handleChange(event) {
        this.setState({
            value: event.target.value,
            error: false
        });
        TestsActions.updateName(this.state.value, this.state.nameType);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({error: nextProps.error})
    }

    render() {
        var style = "";
        if (this.state.error == true) {
            style = "error";
        }

        return (<Input type="text" label={this.state.label} placeholder={this.state.value} bsStyle={style} onChange={this.handleChange.bind(this)}
                       onKeyUp={this.handleChange.bind(this)}/>)
    }
}
