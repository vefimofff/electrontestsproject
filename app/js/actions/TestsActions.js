import AppDispatcher from '../dispatcher/AppDispatcher';
import TestsConstants from '../constants/TestsConstants';

var fs = remote.require('fs');
var console = remote.require('console');

var testsFilePath = "../tests/";


function readJsonFromFile(fileName) {
    var testsJsonAsString = fs.readFileSync(testsFilePath + fileName, 'utf-8');
    return JSON.parse(testsJsonAsString);
}

function readSectionsFromFile() {
    var json = readJsonFromFile("test.txt");
    var sectionsFromFile = json.sections;
    var sectionsForStore = [];

    for (var sectionIndex = 0; sectionIndex < sectionsFromFile.length; sectionIndex++) {
        var sectionTitle = sectionsFromFile[sectionIndex].title;
        var numberOfQuestionsForTest = sectionsFromFile[sectionIndex].numberOfQuestionsForTest;
        var questionsDataArray = sectionsFromFile[sectionIndex].questions.questions;

        var questions = [];
        for (var i = 0; i < questionsDataArray.length; i++) {
            var questionData = questionsDataArray[i];

            var choices = [];
            var choicesDataArray = questionData.choices;
            for (var choiceIndex = 0; choiceIndex < choicesDataArray.length; choiceIndex++) {
                var choiceData = choicesDataArray[choiceIndex];
                var choice = {};
                choice.text = choiceData.text;
                choice.selected = false;
                choices[choiceIndex] = choice;
            }
            questions.push({
                title: questionData.title,
                answer: questionData.correctAnswer,
                categories: questionData.categories,
                choices: choices
            });
        }

        sectionsForStore.push({
            title: sectionTitle,
            numberOfQuestionsForTest: numberOfQuestionsForTest,
            questions: questions
        });
    }

    return sectionsForStore;
}

function getEvaluatedQuestion(correctAnswer, choices) {
    var questionResult;
    var userAnswer = "";

    //to show if user has selected at least once checkbox
    var hasAnyAnswer = false;
    for (var key in choices) {
        if (choices[key].selected) {
            hasAnyAnswer = true;
            //adding "," starting from first value added
            userAnswer += (userAnswer == "" ? "" : ",") + key
        }
    }

    //check if question has multiple correct answers.
    if (correctAnswer.indexOf(",") > -1) {
        if (hasAnyAnswer) {
            //removing all spaces the splitting by ","
            var correctAnswersArray = correctAnswer.replace(/\s+/g, '').split(",");
            var userAnswersArray = userAnswer.replace(/\s+/g, '').split(",");

            questionResult = "correct";
            if (correctAnswersArray.length != userAnswersArray.length) {
                //for sure some answer is missing or extra, so marking question as incorrect
                questionResult = "incorrect";
            } else {
                for (var i = 0; i < correctAnswersArray.length; i++) {
                    //if user's answer does not contain any correctAnswer choice - mark question as incorrect
                    if (userAnswersArray.indexOf(correctAnswersArray[i]) == -1) {
                        questionResult = "incorrect";
                        break;
                    }
                }
            }
        } else {
            questionResult = "skipped";
        }
    } else {
        if (userAnswer === "") {
            questionResult = "skipped";
        } else {
            questionResult = correctAnswer == userAnswer ? "correct" : "incorrect";
        }
    }

    return {
        questionResult: questionResult,
        userAnswer: userAnswer,
        correctAnswer: correctAnswer
    };
}


var TestsActions = {

    updateName: function (text, nameType) {
        var actionType;
        if (nameType == "first_name") {
            actionType = TestsConstants.LOGIN_UPDATE_FIRST_NAME;
        } else if (nameType == "last_name") {
            actionType = TestsConstants.LOGIN_UPDATE_LAST_NAME;
        } else if (nameType == "middle_name") {
            actionType = TestsConstants.LOGIN_UPDATE_MIDDLE_NAME;
        }
        AppDispatcher.dispatch({
            actionType: actionType,
            text: text
        });
    },

    updateCategory: function (text) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.LOGIN_UPDATE_CATEGORY,
            text: text
        });
    },

    updateDayOfBirth: function (day) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.LOGIN_UPDATE_DAY_OF_BIRTH,
            day: day
        });
    },

    updateMonthOfBirth: function (month) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.LOGIN_UPDATE_MONTH_OF_BIRTH,
            month: month
        });
    },

    updateYearOfBirth: function (year) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.LOGIN_UPDATE_YEAR_OF_BIRTH,
            year: year
        });
    },

    readSectionsFromFile: function () {
        var sections = readSectionsFromFile();

        AppDispatcher.dispatch({
            actionType: TestsConstants.CREATE_SECTIONS,
            sections: sections
        });
    },

    updateDisplayedQuestionIndex: function (index) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.SET_DISPLAYED_QUESTION_INDEX,
            index: index
        });
    },

    evaluateQuestion: function (selectedQuestionIndex, correctAnswer, choices) {
        var evaluatedQuestion = getEvaluatedQuestion(correctAnswer, choices);

        AppDispatcher.dispatch({
            actionType: TestsConstants.EVALUATE_QUESTION,
            questionIndex: selectedQuestionIndex,
            evaluatedQuestion: evaluatedQuestion
        });
    },

    choiceClicked: function (selectedQuestionIndex, choiceNumber, inputType) {
        AppDispatcher.dispatch({
            actionType: TestsConstants.CHOICE_CLICKED,
            questionIndex: selectedQuestionIndex,
            choiceNumber: choiceNumber,
            inputType: inputType
        });
    }

};

module.exports = TestsActions;
