import React, { PropTypes, Component } from 'react';
import { Input } from 'react-bootstrap'
import TestsActions from '../actions/TestsActions';

export default
class CategoryInput extends Component {
    constructor(props) {
        super(props);
        this.state = {error: this.props.error};
    }

    handleChange(event) {
        TestsActions.updateCategory(event.target.value);
        this.setState({
            error: false
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({error: nextProps.error})
    }

    //Option values are constant do not change them. The same values are used in Creator component
    render() {
        var style = "";
        if (this.state.error == true) {
            style = "error";
        }

        return <Input type="select" label="Категория должности:" placeholder="select" bsStyle={style} onChange={this.handleChange.bind(this)}>
            <option value="none">Выберите категорию должности</option>
            <option value="management">Руководители</option>
            <option value="specialists">Специалисты</option>
            <option value="providers">Обеспечивающие специалисты</option>
        </Input>;
    }

}
