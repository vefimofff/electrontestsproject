import React, { PropTypes, Component } from 'react';
import { Input } from 'react-bootstrap'
import TestsActions from '../actions/TestsActions';

export default
class DateInput extends Component {
    constructor(props) {
        super(props);
        this.state = {errorDay: this.props.errorDay, errorMonth: this.props.errorMonth, errorYear: this.props.errorYear};
    }

    handleDayChange(event) {
        TestsActions.updateDayOfBirth(event.target.value);
        this.setState({
            errorDay: false
        });
    }

    handleMonthChange(event) {
        TestsActions.updateMonthOfBirth(event.target.value);
        this.setState({
            errorMonth: false
        });
    }

    handleYearChange(event) {
        TestsActions.updateYearOfBirth(event.target.value);
        this.setState({
            errorYear: false
        });
    }

    getDayOptions() {
        var days = [];
        days.push(<option value="0">...</option>);
        for (var i = 1; i <= 31; i++) {
            days.push(<option value={i}>{i}</option>)
        }
        return days;
    }

    getMonthOptions() {
        var months = [];
        months.push(<option value="0">...</option>);
        months.push(<option value="1">Январь</option>);
        months.push(<option value="2">Февраль</option>);
        months.push(<option value="3">Март</option>);
        months.push(<option value="4">Апрель</option>);
        months.push(<option value="5">Май</option>);
        months.push(<option value="6">Июнь</option>);
        months.push(<option value="7">Июль</option>);
        months.push(<option value="8">Август</option>);
        months.push(<option value="9">Сентябрь</option>);
        months.push(<option value="10">Октябрь</option>);
        months.push(<option value="11">Ноябрь</option>);
        months.push(<option value="12">Декабрь</option>);
        return months;
    }

    getYearOptions() {
        var years = [];
        var adultAge = 18;
        years.push(<option value="0">...</option>);
        for (var i = 1930; i <= (2016 - adultAge); i++) {
            years.push(<option value={i}>{i}</option>)
        }
        return years;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({errorDay: nextProps.errorDay, errorMonth: nextProps.errorMonth, errorYear: nextProps.errorYear})
    }

    render() {

        var dayStyle = "";
        if (this.state.errorDay == true) {
            dayStyle = "error";
        }
        var monthStyle = "";
        if (this.state.errorMonth == true) {
            monthStyle = "error";
        }
        var yearStyle = "";
        if (this.state.errorYear == true) {
            yearStyle = "error";
        }


        return <div>
            <p className="dateSelectorsHeader">Дата рождения</p>
            <div id="day" className="daySelector">
                день
                <Input type="select" placeholder="select" bsStyle={dayStyle} onChange={this.handleDayChange.bind(this)}>
                    {this.getDayOptions()}
                </Input>
            </div>
            <div id="month" className="monthSelector marginLeft5">
                месяц
                <Input type="select" placeholder="select" bsStyle={monthStyle} onChange={this.handleMonthChange.bind(this)}>
                    {this.getMonthOptions()}
                </Input>
            </div>
            <div id="year" className="yearSelector marginLeft5">
                год
                <Input type="select" placeholder="select" bsStyle={yearStyle} onChange={this.handleYearChange.bind(this)}>
                    {this.getYearOptions()}
                </Input>
            </div>
        </div>;
    }

}
