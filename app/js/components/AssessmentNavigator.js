import React, {Component} from 'react';
import TestsStore from '../stores/TestsStore';

export default
class AssessmentNavigator extends Component {
    constructor(props) {
        super(props);
    }

    onQuestionNumberClick(questionIndex) {
        this.props.onQuestionNumberClick(questionIndex)
    }

    render() {
        var questionsLineTDs = [];
        for (var questionIndex = 0; questionIndex < this.props.totalNumberOfQuestions; questionIndex++) {
            var isAnswered = TestsStore.isQuestionAnswered(questionIndex);
            var cssClass = "questionNumberInRow";
            if (questionIndex == this.props.selectedQuestionIndex) {
                cssClass = cssClass + " selected";
            } else if (isAnswered) {
                cssClass = cssClass + " answered";
            }

            var questionNumber = questionIndex + 1;
            questionsLineTDs.push(<td onClick={this.onQuestionNumberClick.bind(this, questionIndex)} key={questionNumber} className={cssClass}>{questionNumber}</td>);
        }
        return <table>
            <tr>
                {questionsLineTDs}
            </tr>
        </table>;
    }
}
