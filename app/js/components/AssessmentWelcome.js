import React, {Component} from 'react';
import TestsStore from '../stores/TestsStore';
import TestAction from '../actions/TestsActions';
import { Link } from 'react-router';
import { Button } from 'react-bootstrap'


export default
class AssessmentWelcome extends Component {
    constructor(props) {
        super(props);
        this.state = {firstName: TestsStore.getFirstName(), middleName: TestsStore.getMiddleName()};
    }

    startTest() {
        TestAction.readSectionsFromFile();
        TestAction.updateDisplayedQuestionIndex(0);
    }

    render() {
        return <div className="assessmentWelcomeDiv">
            <div className="headerDivFont">Здравствуйте, {this.state.firstName} {this.state.middleName}</div>
            <Link to="/">изменить имя</Link>

            <div className="marginTop20">
                Вам предстоит пройти тест на:
                <ul>
                    <li>знание основ Конституции Российской Федерации и Федерального конституционного закона «О Правительстве Российской Федерации»;</li>
                    <li>знание норм Федерального закона от 27 июля 2004г. №79-ФЗ "О государственной гражданской службе Российской Федерации";</li>
                    <li>знание основных концепций по вопросам противодействия коррупции;</li>
                    <li>знание Регламента Правительства Российской Федерации и Инструкции по делопроизводству в Аппарате Правительства Российской Федерации;</li>
                    <li>владение основными правилами орфографии, пунктуации, лексическими и грамматическими нормами русского языка</li>
                </ul>
            </div>

            <div className="marginTop20">
                Нажмите кнопку "Старт" для начала теста.
            </div>

            <div className="assessmentWelcomeStartTestButtonDiv">
                <Button bsStyle="primary" bsSize="large" onClick={this.startTest}>Старт</Button>
            </div>
            {this.props.children}
        </div>;
    }

}
