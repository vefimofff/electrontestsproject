require('./app/less/main.less');
'use strict';
import React, { PropTypes, Component } from 'react';

import {IndexRoute, Route, Router} from 'react-router';
import LoginPage from './app/js/components/LoginPage';
import TestPage from './app/js/components/TestPage';

React.render(
        <div>
            <div className="logoDiv">
                <div className="logoTextDiv">Тестирование</div>
            </div>

            <Router>
                <Route path="/">
                    <IndexRoute component={LoginPage}/>
                    <Route path="test" component={TestPage}/>
                </Route>
            </Router>
        </div>, document.getElementById('content'));

