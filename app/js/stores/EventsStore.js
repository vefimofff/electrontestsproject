var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var EventsConstants = require('../constants/EventsConstants');
var assign = require('object-assign');

var fs = remote.require('fs');
var console = remote.require('console');
var _ = require('lodash');

var CHANGE_EVENT = 'change';

var _events = [];

function fillEventsData(events) {
    _events =  _.map(events, _.clone);
}

var EventsStore = assign({}, EventEmitter.prototype, {

    getEvent: function (index) {
        return _events[index];
    },

    getEvents: function () {
        return _events;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case EventsConstants.CREATE_EVENTS:
            fillEventsData(action.events);
            break;

        default:
        // no op
    }

});

module.exports = EventsStore;
