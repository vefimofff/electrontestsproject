import React, {Component} from 'react';
import TestsStore from '../stores/TestsStore';
import TestsActions from '../actions/TestsActions'
import ResultActions from '../actions/ResultActions'
import TestResultReport from './TestResultReportTable';
import { Button, Table } from 'react-bootstrap'

function getTestMark(totalNumberOfQuestions, numberOfCorrectAnswers) {
    var percentageOFCorrectAnswers = numberOfCorrectAnswers / totalNumberOfQuestions;
    var mark = "";
    if (percentageOFCorrectAnswers >= 0.9) {
        return "Отлично";
    } else if (percentageOFCorrectAnswers >= 0.7 && percentageOFCorrectAnswers < 0.9) {
        return "Хорошо";
    } else if (percentageOFCorrectAnswers >= 0.5 && percentageOFCorrectAnswers < 0.7) {
        return "Удовлетворительно";
    } else {
        return "Неудовлетворительно"
    }
}


export default
class AssessmentTestCompleted extends Component {

    constructor(props) {
        super(props);

        //TODO remove this from constructor as this component is not needed in the production version
        var firstName = TestsStore.getFirstName();
        var lastName = TestsStore.getLastName();
        var middleName = TestsStore.getMiddleName();
        var dayOfBirth = TestsStore.getDayOfBirth();
        var monthOfBirth = TestsStore.getMonthOfBirth();
        var yearOfBirth = TestsStore.getYearOfBirth();
        var category = TestsStore.getCategory();

        var questions = TestsStore.getQuestions();
        var evaluatedQuestions = TestsStore.getEvaluatedQuestions();
        var fullName = lastName + " " + firstName + " " + middleName;
        var dateOfBirth = dayOfBirth + "." + monthOfBirth + "." + yearOfBirth;

        ResultActions.createPDFReport(questions, evaluatedQuestions, fullName, dateOfBirth, category);
        ResultActions.createXLSReport(questions, evaluatedQuestions, fullName, dateOfBirth, category);
    }

    render() {
        //var questionGroups = TestsStore.getQuestionGroups();
        //var evaluatedQuestions = TestsStore.getEvaluatedQuestions();
        //var testResultsReportArray = [];
        //
        //for (var i = 0; i < questionGroups.length; i++) {
        //    var correctAnswersCount = 0;
        //    var incorrectAnswersCount = 0;
        //    var skippedAnswersCount = 0;
        //    var numberOfQuestions = 0;
        //
        //    var firstQuestionIndex = questionGroups[i].firstQuestionIndex;
        //    var lastQuestionIndex = questionGroups[i].lastQuestionIndex;
        //
        //    for (var j = firstQuestionIndex; j <= lastQuestionIndex; j++) {
        //        var evaluatedQuestion = evaluatedQuestions[j];
        //
        //        if (evaluatedQuestion == undefined || evaluatedQuestion.questionResult == "skipped") {
        //            skippedAnswersCount++;
        //        } else if (evaluatedQuestion.questionResult == "correct") {
        //            correctAnswersCount++;
        //        } else if (evaluatedQuestion.questionResult == "incorrect") {
        //            incorrectAnswersCount++;
        //        }
        //
        //        numberOfQuestions++;
        //    }
        //
        //    var mark = getTestMark(numberOfQuestions, correctAnswersCount);
        //    var groupTitle = questionGroups[i].title;
        //    testResultsReportArray.push(<TestResultReport title={groupTitle} correctAnswersCount={correctAnswersCount} incorrectAnswersCount={incorrectAnswersCount}
        //                                                  skippedAnswersCount={skippedAnswersCount} mark={mark}/>);
        //}
        // Uncomment code above and add the line below after header div to show results table
        //            {testResultsReportArray}


        return <div className="resultsTableDiv">
            <div className="headerDivFont">Тестирование завершено</div>
            <div className="marginTop50 marginBottom50">Ваши результаты сохранены, можете закрыть программу</div>
        </div>;
    }

}
