import React, {Component} from 'react';
import { Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { Button } from 'react-bootstrap';
import TestsStore from '../stores/TestsStore';
import TestsActions from '../actions/TestsActions'
import AssessmentView from './AssessmentView';
import AssessmentWelcome from './AssessmentWelcome';
import AssessmentTestCompleted from './AssessmentTestCompleted';
import GoToFirstPageButton from './GoToFirstPageButton';

function getDisplayedQuestionIndexFromStore() {
    return {displayedQuestionIndex: TestsStore.getDisplayedQuestionIndex()};
}

export default
class TestPage extends Component {

    constructor(props) {
        super(props);
        this.state = getDisplayedQuestionIndexFromStore();
    }

    componentDidMount() {
        TestsStore.addChangeListener(this.updateDisplayedQuestionIndex.bind(this));
    }

    componentWillUnmount() {
        TestsStore.removeChangeListener(this.updateDisplayedQuestionIndex.bind(this));
    }

    render() {
        var view;
        if (this.state.displayedQuestionIndex == -1) {
            view = <AssessmentWelcome/>;
        } else if (this.state.displayedQuestionIndex > TestsStore.getNumberOfQuestions() - 1) {
            view = <AssessmentTestCompleted/>;
        } else {
            view = <AssessmentView/>;
        }

        return (
                <div className="centerQuestionDiv">
                    <div className="marginTop20">
                        {view}
                    </div>

                    <GoToFirstPageButton/>
                </div>
        );
    }

    updateDisplayedQuestionIndex() {
        this.setState(getDisplayedQuestionIndexFromStore());
    }

}