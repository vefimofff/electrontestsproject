import React, {Component} from 'react';
import { Input, Panel, Button } from 'react-bootstrap';

export default
class Question extends Component {
    constructor(props) {
        super(props);
    }

    handleRadioChange(selectedChoiceNumber, event) {
        this.props.onChoiceClick(this.props.index, selectedChoiceNumber, "radio");
    }

    handleCheckboxChange(checkBoxNumber, event) {
        this.props.onChoiceClick(this.props.index, checkBoxNumber, "checkbox");
    }

    onNextQuestionClick(event) {
        this.props.nextQuestionAction();
    }

    onPrevQuestionClick(event) {
        this.props.prevQuestionAction();
    }

    render() {
        var questionData = this.props.questionData;
        var questionHeader = "Вопрос " + (this.props.index + 1);

        var choices = questionData.choices;

        var choicesUI = [];
        var name = "question_" + questionData.index + "_choice";
        var ref = "question_" + questionData.index + "_choice_";
        var hasMultipleAnswers = this.props.hasMultipleAnswers;

        for (var i = 0; i < choices.length; i++) {
            ref = ref + i;
            if (hasMultipleAnswers) {
                if (choices[i].selected) {
                    choicesUI.push(<Input type="checkbox" name={name} ref={ref} key={i} label={choices[i].text} checked onClick={this.handleCheckboxChange.bind(this, i)}
                                          readOnly/>);
                } else {
                    choicesUI.push(<Input type="checkbox" name={name} ref={ref} key={i} label={choices[i].text} onClick={this.handleCheckboxChange.bind(this, i)} readOnly/>);
                }
            } else {
                if (choices[i].selected) {
                    choicesUI.push(<Input type="radio" name={name} ref={ref} key={i} label={choices[i].text} checked onClick={this.handleRadioChange.bind(this, i)} readOnly/>);
                } else {
                    choicesUI.push(<Input type="radio" name={name} ref={ref} key={i} label={choices[i].text} onClick={this.handleRadioChange.bind(this, i)} readOnly/>);
                }
            }
        }

        var navigationButtons = [];
        if (this.props.isFirst) {
            navigationButtons.push(<Button bsStyle="primary" onClick={this.onNextQuestionClick.bind(this)}>Следующий вопрос</Button>);
        } else if (this.props.isLast) {
            navigationButtons.push(<Button bsStyle="primary" onClick={this.onPrevQuestionClick.bind(this)}>Предыдущий вопрос</Button>);
            navigationButtons.push(<Button bsStyle="success" className="marginLeft20" onClick={this.onNextQuestionClick.bind(this)}>Завершить тестирование</Button>);
        } else {
            navigationButtons.push(<Button bsStyle="primary" className="marginLeft20" onClick={this.onNextQuestionClick.bind(this)}>Следующий вопрос</Button>);
            navigationButtons.push(<Button bsStyle="primary" className="floatLeft" onClick={this.onPrevQuestionClick.bind(this)}>Предыдущий вопрос</Button>);
        }

        return <div>
            <Panel header={questionHeader} bsStyle="primary">
                <div>
                    {questionData.title}
                </div>
                <form className="marginTop20">
                    {choicesUI}
                </form>
            </Panel>

            <div className="questionNavButtonsDiv">
                {navigationButtons}
            </div>
        </div>;
    }
}