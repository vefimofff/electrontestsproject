import React, {Component} from 'react';
import { LinkContainer } from 'react-router-bootstrap'
import TestsActions from '../actions/TestsActions'
import TestsStore from '../stores/TestsStore'
import Question from './Question'

export default
class QuestionContainer extends Component {
    constructor(props) {
        super(props);
        TestsActions.updateDisplayedQuestionIndex(0);
    }


    render() {
        var index = this.props.questionIndex;
        var isFirsQuestion = index == 0;
        var isLastQuestion = index == (TestsStore.getNumberOfQuestions() - 1);

        var questionFromStore = TestsStore.getQuestion(index);

        console.log("QuestionContainer. questionFromStore="+ questionFromStore.title)

        var hasMultipleAnswers = TestsStore.hasMultipleAnswers(index);
        return <div>
            <Question key={index} index={index} questionData={questionFromStore} hasMultipleAnswers={hasMultipleAnswers} isFirst={isFirsQuestion} isLast={isLastQuestion}
                      prevQuestionAction={this.props.prevQuestionAction} nextQuestionAction={this.props.nextQuestionAction} onChoiceClick={this.props.onChoiceClick}/>
        </div>;
    }

}
