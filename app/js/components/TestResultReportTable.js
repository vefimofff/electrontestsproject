import React, {Component} from 'react';
import { Button, Table } from 'react-bootstrap'

export default
class TestResultReport extends Component {

    render() {
        var markStyle = this.props.mark == "Неудовлетворительно" ? "badMarkStyle resultsNumbersTd" : "goodMarkStyle resultsNumbersTd";

        return <Table striped condensed hover className="resultsTable">
            <thead>
            <tr>
                <th>{this.props.title}</th>
                <th className={markStyle}>{this.props.mark}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td className="resultsLabelsTd">Правильных ответов:</td>
                <td className="resultsNumbersTd"><span className="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span> {this.props.correctAnswersCount}</td>
            </tr>
            <tr>
                <td className="resultsLabelsTd">Неверных ответов:</td>
                <td className="resultsNumbersTd"><span className="glyphicon glyphicon-thumbs-down" aria-hidden="true"></span> {this.props.incorrectAnswersCount}</td>
            </tr>
            <tr>
                <td className="resultsLabelsTd">Неотвеченных вопросов:</td>
                <td className="resultsNumbersTd"><span className="glyphicon glyphicon-question-sign" aria-hidden="true"></span> {this.props.skippedAnswersCount}</td>
            </tr>
            </tbody>
        </Table>;
    }
}