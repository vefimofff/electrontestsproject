var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var TestsConstants = require('../constants/TestsConstants');
var assign = require('object-assign');

var fs = remote.require('fs');
var console = remote.require('console');
var _ = require('lodash');

var CHANGE_EVENT = 'change';

var _firstName;
var _lastName;
var _middleName;
var _category;
var _dayOfBirth;
var _monthOfBirth;
var _yearOfBirth;

var _questions = [];
var _evaluatedQuestions = [];
//to store different groups of questions. Like "constitution", "anti corruption", "law basis", etc.
var _questionGroups = [];

//index of the question being currently shown. By default showing welcome screen.
var _displayedQuestionIndex = -1;

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there are remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function selectRandomQuestions(allQuestions, numberOfQuestionsToSelect) {
    var shuffledQuestions = shuffle(allQuestions);

    if (shuffledQuestions.length <= numberOfQuestionsToSelect) {
        return shuffledQuestions;
    } else {
        return shuffledQuestions.slice(0, numberOfQuestionsToSelect);
    }
}

function fillSectionsData(sections) {
    setQuestionsAndGroups(sections);
}

function setQuestionsAndGroups(sections) {
    var allQuestions = [];

    var startIndex = 0;
    var endIndex = 0;
    for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++) {
        var section = sections[sectionIndex];
        var allQuestionsForSection = section.questions;
        var allQuestionsForCategory = [];

        for (var questionIndex = 0; questionIndex < allQuestionsForSection.length; questionIndex++) {
            var question = allQuestionsForSection[questionIndex];

            if (question.categories.management && _category == "management"
                    || question.categories.specialists && _category == "specialists"
                    || question.categories.providers && _category == "providers") {
                console.log('Adding question for category=' + _category);
                allQuestionsForCategory.push(question);
            }
        }

        var shuffledQuestionsForCategory = selectRandomQuestions(allQuestionsForCategory, section.numberOfQuestionsForTest);
        allQuestions = allQuestions.concat(shuffledQuestionsForCategory);

        endIndex = startIndex + shuffledQuestionsForCategory.length - 1;
        var questionGroup = {
            title: section.title,
            firstQuestionIndex: startIndex,
            lastQuestionIndex: endIndex
        };
        _questionGroups.push(questionGroup);

        startIndex = allQuestions.length;
    }

    _questions =  _.map(allQuestions, _.clone);
}

function setDisplayedQuestionIndex(index) {
    _displayedQuestionIndex = index;
}

function setLoginFirstName(firstName) {
    _firstName = firstName;
}

function setLoginLastName(lastName) {
    _lastName = lastName;
}

function setLoginMiddleName(middleName) {
    _middleName = middleName;
}

function setLoginCategory(category) {
    _category = category;
}

function setDayOfBirth(dayOfBirth) {
    _dayOfBirth = dayOfBirth;
}

function setMonthOfBirth(monthOfBirth) {
    _monthOfBirth = monthOfBirth;
}

function setYearOfBirth(yearOfBirth) {
    _yearOfBirth = yearOfBirth;
}

/**
 *
 * @param questionIndex - index of questions to change choices selected value. Starts from 0
 * @param choiceIndex - number of choice to change selected value. Starts from 0.
 * @param inputType - "radio" - as only one radio button can have (selected == true) we need to set others choices selected to false.
 *                  - "checkbox" - just change the value for given checkbox
 */
function choiceClicked(questionIndex, choiceIndex, inputType) {
    var choices = _questions[questionIndex].choices;
    if (inputType == "checkbox") {
        choices[choiceIndex].selected = choices[choiceIndex].selected == false;
    } else {
        for (var i = 0; i < choices.length; i++) {
            choices[i].selected = i == choiceIndex;
        }
    }
}

function evaluateQuestion(questionIndex, evaluatedQuestion) {
    _evaluatedQuestions[questionIndex] = evaluatedQuestion;
}

var TestsStore = assign({}, EventEmitter.prototype, {
    getFirstName: function () {
        return _firstName;
    },

    getLastName: function () {
        return _lastName;
    },

    getMiddleName: function () {
        return _middleName;
    },

    getCategory: function () {
        return _category;
    },

    getDayOfBirth: function () {
        return _dayOfBirth;
    },

    getMonthOfBirth: function () {
        return _monthOfBirth;
    },

    getYearOfBirth: function () {
        return _yearOfBirth;
    },

    getQuestion: function (index) {
        return _questions[index];
    },

    hasMultipleAnswers: function (index) {
        return _questions[index].answer.indexOf(",") > -1;
    },

    /**
     * If at least one choice has "selected" value equal to TRUE it means user has made an attempt
     * @param index question index
     * @returns {boolean}
     */
    isQuestionAnswered: function (index) {
        var question = _questions[index];
        var choices = question.choices;
        for (var key in choices) {
            if (choices[key].selected) {
                return true;
            }
        }
        return false;
    },

    getNumberOfQuestions: function () {
        return _questions.length;
    },

    getEvaluatedQuestions: function () {
        return _evaluatedQuestions;
    },

    getSkippedQuestionsNumbers: function () {
        var skippedQuestionsNumbers = [];

        for (var i = 0; i < _evaluatedQuestions.length; i++) {
            if (_evaluatedQuestions[i].questionResult == "skipped") {
                //numbers starting from 1
                skippedQuestionsNumbers.push(i + 1);
            }
        }
        return skippedQuestionsNumbers;
    },

    getQuestionGroups: function () {
        return _questionGroups;
    },

    getQuestionGroupTitleForQuestion: function (index) {
        for (var key in _questionGroups) {
            if (index >= _questionGroups[key].firstQuestionIndex && index <= _questionGroups[key].lastQuestionIndex) {
                return _questionGroups[key].title;
            }
        }
        return "--";
    },

    getQuestions: function () {
        return _questions;
    },

    getDisplayedQuestionIndex: function () {
        return _displayedQuestionIndex;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case TestsConstants.CREATE_SECTIONS:
            fillSectionsData(action.sections);
            TestsStore.emitChange();
            break;

        case TestsConstants.SET_DISPLAYED_QUESTION_INDEX:
            var index = action.index;
            setDisplayedQuestionIndex(index);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_FIRST_NAME:
            setLoginFirstName(action.text);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_LAST_NAME:
            setLoginLastName(action.text);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_MIDDLE_NAME:
            setLoginMiddleName(action.text);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_CATEGORY:
            setLoginCategory(action.text);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_DAY_OF_BIRTH:
            setDayOfBirth(action.day);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_MONTH_OF_BIRTH:
            setMonthOfBirth(action.month);
            TestsStore.emitChange();
            break;

        case TestsConstants.LOGIN_UPDATE_YEAR_OF_BIRTH:
            setYearOfBirth(action.year);
            TestsStore.emitChange();
            break;

        case TestsConstants.EVALUATE_QUESTION:
            evaluateQuestion(action.questionIndex, action.evaluatedQuestion);
            TestsStore.emitChange();
            break;

        case TestsConstants.CHOICE_CLICKED:
            choiceClicked(action.questionIndex, action.choiceNumber, action.inputType);
            TestsStore.emitChange();
            break;

        default:
        // no op
    }

});

module.exports = TestsStore;
